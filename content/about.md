---
title: "About"
date: 2017-09-23T12:21:40-05:00
draft: false
menu:
    main:
        weight: 20
---
<p>How many is that? was inspired by work on <a href="http://www.eachofushasaname.org">Each of Us Has a Name</a>, a Holocaust remembrance project. The purpose of Each of Us Has a Name is to memorialize and recite Kaddish (the Jewish prayer for mourning) for the Jewish victims of the Holocaust on an individual basis. The ultimate goal is to complete a database of six million names. This is a lofty goal — six million is an almost unfathomable number. This is the case with the numbers associated with many tragedies. We cannot even begin to truly understand how many human lives are and have been affected by terrible things throughout history. How many is that? is an attempt to put these numbers into better perspective so that we can truly empathize with individuals’ tragedies instead glossing over them with just numbers and statistics.</p>

<div id="social">
  <h3><small>Get in touch</small></h3>
  <a href="/contact" class="email" title="Contact us"><i class="fa fa-envelope"></i></a>
  <a href="https://www.facebook.com/howmanyisthat" target="_blank" class="facebook" title="Like us"><i class="fa fa-facebook"></i></a>
  <a href="https://twitter.com/HowManyIs" target="_blank" class="twitter" title="Follow us"><i class="fa fa-twitter"></i></a>
</div>

