var shell = require('shelljs');

var jsonfile = require('jsonfile');

 var injusticeFile = './data/injustices.json';
 var locationFile = './data/locations.json';


 var injustices = jsonfile.readFileSync(injusticeFile);


 var injusticeID = process.argv[2];

 var injustice = null;

 injustices.forEach((injustice)=>{
    shell.exec("rm -rf content/compare");
    shell.echo("Building "+injustice.title);
    shell.exec("node generate-pages "+injustice.id);
    shell.exec("hugo");
 })
