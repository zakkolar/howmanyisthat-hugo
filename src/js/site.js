function getZip(){
    return localStorage.getItem('zipcode');
}

function setZip(zip, success, fail){
    if(success == undefined){
        success = function(){};
    }
    if(fail == undefined){
        fail = function(){};
    }
    $.get('/compare/Holocaust-all/'+zip).done(function(){
        localStorage.setItem('zipcode',zip);
        success();
    }).fail(function(){
        alert("The zip code you entered ("+zip+") was not found in the database. If you mistyped your zip code, please try again. If not, please contact us via the contact page so that we can add your zip code. For now, How many is that? only works with zip codes inside the United States.");
        fail();
    })

}