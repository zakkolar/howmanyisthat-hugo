 var jsonfile = require('jsonfile');

 var injusticeFile = './data/injustices.json';
 var locationFile = './data/locations.json';


 var injustices = jsonfile.readFileSync(injusticeFile);
 var locations = jsonfile.readFileSync(locationFile);

 var fs = require('fs-sync');


 var injusticeID = process.argv[2];

 var injustice = null;

 injustices.forEach((curInjustice)=>{
    if(curInjustice.id==injusticeID){
        injustice = curInjustice;
    }
 })

 locations.forEach((location)=>{
              var file = './content/compare/'+injustice.id+'/'+location.zip_code+'.md';
              var obj = {
                  draft: false,
                  title: "How many is "+injustice.number_word+"?",
                  show_header:false,
                  injustice: injustice,
                  location: location
              };

             fs.write(file, JSON.stringify(obj, null, "\t"));

          })
