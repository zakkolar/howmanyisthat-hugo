var gulp         = require("gulp"),
    less         = require("gulp-less"),
    autoprefixer = require("gulp-autoprefixer"),
    concat       = require('gulp-concat'),
    cleanCSS     = require('gulp-clean-css');


gulp.task('less', function () {
  return gulp.src('./src/less/site.less')
    .pipe(less())
    .pipe(autoprefixer({
        browsers: ["last 5 versions"]
     }))
     .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./static/css'));
});

gulp.task('js', function() {
  return gulp.src('./src/js/**/*.js')
    .pipe(concat('site.js'))
    .pipe(gulp.dest('./static/js'));
});

gulp.task("watch", ["less", "js"], function(){
    gulp.watch("./src/less/**/*.less", ["less"]);
    gulp.watch("./src/js/**/*.js", ["js"]);
});

gulp.task("build", ["less", "js"]);